import React from "react";
import ReactDOM from "react-dom";
import Titles from "./components/Titles";
import Characters from "./components/Characters";




class App extends React.Component {
    
    constructor(){
        super ();
        this.state={
            data:[],
        }
    }
    
    componentDidMount()
    {
        fetch('https://swapi.co/api/people/').then((Response)=>Response.json()).then((findresponse)=>{
            
            this.setState({
                data:findresponse.results,
            })
        })
    }

   
    render() {
        
       
        
        return (
                 
            <div>
    <Titles/>
            {
                this.state.data.map((dynamicData,i)=>
                <Characters name={dynamicData.name}/>
                
                        
                )
            
          
            }   



            </div>

        );

    }
};

export default App;